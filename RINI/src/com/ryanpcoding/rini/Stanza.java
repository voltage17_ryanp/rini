/*
 * Stanza.java						9/21/2013
 */
package com.ryanpcoding.rini;

import java.util.Map;

/**
 * Stanza is a section in the ".ini" file that holds certain information.
 * They are marked with [ and ] in their names.
 * 
 * @author Ryan17
 *
 */
public class Stanza {

	/**
	 * Name of the stanza
	 */
	private String name;
	
	/**
	 * Holds the contents of the stanza.
	 */
	private Map<String, String> contents;
	
	/**
	 * Constructor - creates the stanza.
	 * @param name name of the stanza
	 */
	public Stanza(String name) {
		this.name = name;
	}
	
	public Stanza(String name, Map<String, String> contents) {
		this(name);
		this.putContents(contents);
	}
	
	public void putContents(Map<String, String> contents) {
		this.contents = contents;
	}
	
	/**
	 * Returns the name of this stanza
	 * @return name of this stanza
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets the value of the string index in the content map.
	 * @param index String index to get the value of
	 * @return the value of the string index
	 */
	public String getValue(String index) {
		return this.contents.get(index);
	}
	
	public Map<String, String> getContents() {
		return this.contents;
	}
	
	
	
}
