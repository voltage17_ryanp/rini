/*
 * MalformedINIFileExcepion.java				9/22/2013
 */
package com.ryanpcoding.rini;

/**
 * This is an error class for the RINI file system.
 * 
 * This occurs when a file, like the one below, is malformed.
 * 
 * Take for example a normal file:
 * 
 * [Stanza]
 * name = value1
 * name2 = value2
 * 
 * 
 * That is a valid .ini file format. The following is not:
 * 
 * 
 * name = value1
 * name2 = value2
 * 
 * This error will be thrown when the stanza is missing for the name and value.
 * @author Ryan17
 *
 */
@SuppressWarnings("serial")
public class MalformedINIFileException extends Exception {

	/**
	 * Constructor.
	 * 
	 * @param msg The message to be displayed on the error.
	 */
	public MalformedINIFileException(String msg) {
		super(msg);
	}
}
