/*
 * Copyright (c) Ryan P
 * RINI.java							9/21/2013
 */
package com.ryanpcoding.rini;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * RINI is an API for managing local INI files for the vChat program.
 * 
 * <blockquote>
 * Here is a working example of the RINI application:
 * 
 * Contents of code before:
 * 
	<pre>
	[Stanza1]
	key1 = value1
	
	
	[Stanza2]
	key1 = value1
	</pre>
	 * The following code was executed:
	{@code
	RINI r = new RINI("C:\\Users\\Ryan17\\Desktop\\testing.ini");
	ArrayList<Stanza> stanzas = r.getContents();
	
	Map<String, String> a = new HashMap<String, String>();
	a.put("test", "answer");
	a.put("test2", "answer2");
	stanzas.add(new Stanza("Testing", a));
	Map<String, String> map = r.getContentsByStanza("Stanza1");
	map.put("key3", "value3");
	map.put("key1", "value2");
	map = r.getContentsByStanza("Stanza2");
	map.put("key4", "value4");
	
	
	r.setContents(stanzas);
	}
	</pre>
	 * 
	 * Which resulted in the following file:
	<pre>
	[Stanza1]
	key3 = value3
	key1 = value2
	
	[Stanza2]
	key4 = value4
	key1 = value1
	
	[Testing]
	test = answer
	test2 = answer2
	</pre>
	</blockquote>
 * 
 * @author Ryan17
 *
 */
public class RINI {

	private File file;
	
	private ArrayList<Stanza> stanzas;
	
	private final String NEW_LINE = System.getProperty("line.separator");
	
	
	/**
	 * Constructor.
	 * 
	 * If the file exists, it will open it and parse it all for information.
	 * 
	 * If the file does not exist, it will create it.
	 * 
	 * @param loc String location of the file including the name 
	 * 			(.ini is not required)
	 */
	public RINI(String loc) {
		//Check to see if it has .ini in the file name. If not, add it.
		this(loc, ".ini");
	}
	
	/**
	 * Creates a new RINI instance of a file with given file suffix
	 * @param loc Location of the file
	 * @param suffix The suffix of the file (File extension)
	 */
	public RINI(String loc, String suffix) {
		
		if (loc.indexOf(suffix) == -1) {
			//Suffix isn't found, add it on
			loc += suffix;
		}
		
		//The file name is correct so far, so go ahead and create the file.
		this.file = new File(loc);
		
		init();
		
	}
	
	public RINI(File f, String suffix) {
		String loc = f.getAbsolutePath();
		System.out.println(loc);
		
		if (loc.indexOf(suffix) == -1) {
			loc += suffix;
		}
		
		this.file = new File(loc);
		
		init();
	}
	
	private void init() {
		
		this.stanzas = new ArrayList<Stanza>();
		
		if (!this.file.exists()) {
			//The file exists, so go ahead and read it.
			try {
				this.file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			parse();
		} catch (MalformedINIFileException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Parse the ini file for information.
	 * @throws MalformedINIFileException when the .ini file is incorrectly labeled.
	 */
	public void parse() throws MalformedINIFileException {
		//First, clear what ever we may already have.
		stanzas.clear();
		
		//Now, start reading it line by line. It's the only way!
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String line = null;
			Map<String, String> contents = new HashMap<String, String>();
			Stanza stanza = null;
			String stanzaName = null;
			
			//System.out.println(System.nanoTime());
			
			//Loop for reading it all. Read it line by line.
			while ( (line = br.readLine()) != null) {
				if (line.length() == 0) {
					continue;
				}
				//Start parsing the file for what we need.
				//Check for [Stanza] without ' ' (spaces)
				
				if (line.indexOf("[") == 0
						&& line.indexOf("]") == (line.length()-1)
						&& line.indexOf("=") == -1) {
					
					if (stanzaName != null && stanza != null) {
						//There's a running stanza and we have a new stanza!
						stanza.putContents(contents);
						
						this.stanzas.add(stanza);
						contents = new HashMap<String, String>();
						stanza = null;
						stanzaName = null;
					}
					
					stanzaName = line.substring(1, line.length()-1);
					stanza = new Stanza(stanzaName);
				} else {
					//First, make sure it had a stanza.
					if (stanza == null) {
						//Had no stanza
						throw new MalformedINIFileException("The line " 
								+ line.trim() 
								+ " does not have a stanza. "
								+ "Consider reformatting the .ini file.");
					} else {
						
						//This is a key = value pair
						//First, split the string into the two.
						String[] split = line.split("=");

							
						//Add it to the contents
						//Format: contents.put(key, value);
						contents.put(split[0].trim(), getRestOfArray(split, 1));
						//System.out.println(contents);
					
					}
				}
			}
			
			//Finish up any stanza that was currently running
			if (stanzaName != null && stanza != null) {
				//There's a running stanza and we have a new stanza!
				stanza.putContents(contents);
				
				this.stanzas.add(stanza);
			}
			
			
			
			//Close the streams
			br.close();
			fr.close();
			
			//System.out.println(System.nanoTime());
			
			//Make the virtual machine want to garbage collect...
			System.gc();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes the file with the current contents and their stanzas
	 */
	public void write() {
		
		try {
			//Go ahead and prepare it for writing
			FileWriter fw = new FileWriter(this.file);
		
			//We'll be writing on a per-stanza basis.
			//Start a loop for each stanza.
			//First, declare each Stanza's contents as a hash map
			Map<String, String> map;
			
			
			//Loop
			for (Stanza s: this.stanzas) {
				//Now, get the contents for this specific stanza.
				map = s.getContents();
				
				//First, write the stanza's name
				fw.write("[" + s.getName() + "]" + NEW_LINE);
				
				for (Map.Entry<String, String> entry : map.entrySet()) {
					String key = entry.getKey().trim();
					String value = entry.getValue().trim();
					
					fw.write(key + " = " + value + NEW_LINE);
					
					//System.out.println("Writing for stanza " + s.getName() + ": " + key + " = " + value);
				}
				//Write the next line (the break between stanzas)
				fw.write("" + NEW_LINE);
			}
			
			//Done looping, close the streams
			fw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Again, make it want to collect garbage.
		System.gc();
	}
	
	/**
	 * Sets the contents to this .ini file
	 * 
	 * @param contents
	 */
	public void setContents(ArrayList<Stanza> contents) {
		this.stanzas = contents;
		write();
	}
	
	/**
	 * Gets the contents of this .ini file
	 * @return an array list of stanzas
	 */
	public ArrayList<Stanza> getContents() {
		return this.stanzas;
	}
	
	/**
	 * Gets contents by stanza name
	 * @param name
	 * @return the contents of the stanza
	 */
	public Map<String, String> getContentsByStanza(String name) {
		for (Stanza s : this.stanzas) {
			if (s.getName().equalsIgnoreCase(name)) {
				return s.getContents();
			}
		}
		return null;
	}
	
	/**
	 * Gets the rest of the array from the starting position.
	 * @param array Array to get the string from
	 * @param start The start index
	 * @return the array as a string from index start
	 */
	private String getRestOfArray(String[] array, int start) {
	
		StringBuffer buff = new StringBuffer();
		
		for (int i = start; i < array.length; i++) {
			buff.append(array[i]);
		}
		
		return buff.toString();
	}
	
	/**
	 * Returns the stanza count (number of stanzas in this file)
	 * @return stanza count
	 */
	public int getStanzaCount() {
		return this.stanzas.size();
	}
	
}
